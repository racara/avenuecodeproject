﻿using ProjetoAvenueCodeV2.Util;

namespace ProjetoAvenueCodeV2
{
    class ACTask : ACBase
    {
        private static Element _element;
        private static ACPage _ac;

        public ACTask()
        {
            _ac = new ACPage();
            _element = new Element();
        }
        //---------------------SEÇÃO DE RECOMENDACOES---------------------------

        public void InserirEmail(string value)
        {
            _element.SetText(_ac.Email(), value, "Inserir email");
        }

        public void InserirSenha(string value)
        {
            _element.SetText(_ac.Senha(), value, "Inserir Senha");
        }

        public void EntrarNoSistema()
        {
            _element.Click(_ac.BotaoEntrar(), "Botão Entrar");
        }

        public void ClicarNoMyTask()
        {
            _element.Click(_ac.ClicarMyTask(), "Botão My Task");
        }

        public void InserirTaskEnter(string value)
        {
            _element.SetTextAndEnter(_ac.InserirTask(), value, "Inserir Senha");
        }

        public void InserirTask(string value)
        {
            _element.SetText(_ac.InserirTask(), value, "Inserir Tarefa");
        }

        public void ClicarNoMyTaskButton()
        {
            _element.Click(_ac.BotaoInserir(), "Botão Inserir");
        }

        public void ClicarNoManageSubtaskButton()
        {
            _element.Click(_ac.ManageSubtask(), "Botão Inserir");
        }

        public void InserirDescricaoSubtask(string value)
        {
            _element.SetText(_ac.InserirTask(), value, "Inserir Descricao");
        }

        public void InserirDataVencimento(string value)
        {
            _element.SetText(_ac.InserirTask(), value, "Inserir Descricao");
        }

        public void InserirDescricaoSubtaskEnter(string value)
        {
            _element.SetTextAndEnter(_ac.InserirTask(), value, "Inserir Descricao");
        }

        public void InserirDataVencimentoEnter(string value)
        {
            _element.SetTextAndEnter(_ac.InserirTask(), value, "Inserir Descricao");
        }

        public void ClicarEmAddSubtask()
        {
            _element.Click(_ac.AddSubtask(), "Add subtask button");
        }

    }
}
