﻿using NUnit.Framework;
using NUnit.Framework.Internal;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoAvenueCodeV2
{
    class ACBase
    {
        private readonly ACTask _ac;
        protected static string CurrentWindow;
        protected static IWebDriver Selenium;
        protected static int ContadorImagens = 0;
        protected static Logger Log;
        protected static string LogFolder;
        protected static string LogFileResult;
        protected static string LogFileTest;
        protected static string PathImageTest;
        protected static string ImagesFolder;

        [TestFixtureSetUp]
        public void BeforeClass()
        {


            Selenium = new ChromeDriver();
            Selenium.Manage().Window.Maximize();
            Selenium.Navigate().GoToUrl("http://qa-test.avenuecode.com/users/sign_in");


        }

    }
}

