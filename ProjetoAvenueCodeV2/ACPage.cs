﻿using OpenQA.Selenium;
using ProjetoAvenueCodeV2.Util;

namespace ProjetoAvenueCodeV2
{
    class ACPage : ACBase
    {
        private readonly Element _element;

        public ACPage()
        {
            _element = new Element();
        }
        public IWebElement Email()
        {
            return _element.FindById("user_email", "Insirir Email");
        }

        public IWebElement Senha()
        {
            return _element.FindById("user_password", "Insirir Senha");
        }

        public IWebElement BotaoEntrar()
        {
            return _element.FindByName("commit", "Insirir Senha");
        }

        public IWebElement ClicarMyTask()
        {
            return _element.FindByCss("body > div.container > div.jumbotron.grey-jumbotron > center > a", "Clicar em My Task");
        }

        public IWebElement InserirTask()
        {
            return _element.FindById("new_task", "My Task textbox");
        }

        public IWebElement BotaoInserir()
        {
            return _element.FindByCss("body > div.container > div.task_container.ng-scope > div.well > form > div.input-group > span", "Botao My Task");
        }

        public IWebElement ManageSubtask()
        {
            return _element.FindByCss("body > div.container > div.task_container.ng-scope > div.bs-example > div > table > tbody > tr:nth-child(1) > td:nth-child(4) > button", "Botao Manage Subtask");
        }

        public IWebElement SubtaskDescription()
        {
            return _element.FindById("new_sub_task", "Inserir descrição da subtask");
        }

        public IWebElement DueDate()
        {
            return _element.FindById("dueDate", "Inserir data de termino");
        }

        public IWebElement AddSubtask()
        {
            return _element.FindById("add-subtask", "Adicionar Subtask");
        }

    }
}
