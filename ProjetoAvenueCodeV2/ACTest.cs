﻿using System;
using NUnit.Framework;
using NUnit.Compatibility;
using OpenQA.Selenium;
using ProjetoAvenueCodeV2.Util;

namespace ProjetoAvenueCodeV2
{
    class ACTest : ACBase
    {
        private readonly ACTask _ac;
        private readonly CommonActions _common;
        private static Element _element;

        public ACTest()
        {

            _ac = new ACTask();
            _common = new CommonActions();
            _element = new Element();
        }
        [Test]
        public void Tc001VerificarMensagemInicial()
        //Esse teste verifica a mensagem inicial que deve ser apresentada
        {
            try
            {
                _ac.InserirEmail("rzdasil@gmail.com");
                _ac.InserirSenha("12345678");
                _ac.EntrarNoSistema();
                string frasecorreta = _element.Find(By.CssSelector("body > div.container > h1"), "Mensagem").Text;
                if (frasecorreta.Equals("HeyRafael, this is your todo list for today:’"))
                {
                    Console.WriteLine("A mensagem inicial está correta");
                    Assert.IsTrue(true);
                }
                else
                {
                    Console.WriteLine("A mensagem inicial não está correta");
                    Assert.IsFalse(false);
                    throw new Exception("A mensagem não está correta");
                }   
            }

            catch (Exception e)
            {
                _common.LogFailedTest(e.ToString());
            }
        }
        [Test]
        public void Tc002CriarTaskMenosDe3Caracteres()
        //Esse teste visa verifcar se uma task é criada mesmo inserindo typy
        //newtask menos de 3 caracteres
        {
            try
            {
                _ac.InserirEmail("rzdasil@gmail.com");
                _ac.InserirSenha("12345678");
                _ac.EntrarNoSistema();
                _ac.InserirTaskEnter("ab");
                bool task = _element.IsElementPresentByCss("body > div.container > div.task_container.ng-scope > div.bs-example > div > table > tbody > tr > td.task_body.col-md-7 > a");
                if (task.Equals(true))
                {
                    Console.WriteLine("A task foi gerada");
                    throw new Exception("A task foi gerada"); ;
                }
                else
                {
                    Console.WriteLine("A task não foi gerada");
                    Assert.IsFalse(false);

                }
            }

            catch (Exception e)
            {
                _common.LogFailedTest(e.ToString());
            }
        }

        [Test]
        public void Tc003CriarTaskMaisDe250Caracteres()
        //Esse teste visa verifcar se uma task é criada mesmo inserindo typy
        //newtask mais de 250 caracteres
        {
            try
            {
                _ac.InserirEmail("rzdasil@gmail.com");
                _ac.InserirSenha("12345678");
                _ac.EntrarNoSistema();
                _ac.InserirTaskEnter("123456789012345678901234567890123456789012345678901234567890" + 
                    "123456789012345678901234567890123456789012345678901234567890123" + 
                    "45678901234567890123456789012345678901234567890123456789012345"
                  + "6789012345678901234567890123456789012345678901234567890123412345dsdas");
                
                bool task = _element.IsElementPresentByCss("body > div.container > div.task_container.ng-scope > div.bs-example > div > table > tbody > tr:nth-child(2) > td.task_body.col-md-7");
                if (task.Equals(true))
                {
                    Console.WriteLine("A task foi gerada");
                    throw new Exception("A subtask não deveria ser gerada pois a descrição contém mais de 250 caracteres"); ;
                }
                else
                {
                    Console.WriteLine("A task não foi gerada");
                    Assert.IsFalse(false);

                }
            }

            catch (Exception e)
            {
                _common.LogFailedTest(e.ToString());
            }
        }
        [Test]
        public void Tc004CriarSubtaskSemPreencherOsCamposDescricaoeData()
        //Esse teste verifica o comportamento do sistema quando se tenta
        //criar uma subtask sem preencher os campos obrigatorios
        
        {
            try
            {
                _ac.InserirEmail("rzdasil@gmail.com");
                _ac.InserirSenha("12345678");
                _ac.EntrarNoSistema();
                _ac.InserirTaskEnter("Teste");
                _ac.ClicarNoManageSubtaskButton();
                _ac.ClicarEmAddSubtask();
                bool task = _element.IsElementPresentByCss("body > div.modal.fade.ng-isolate-scope.in > div > div > div.modal-body.ng-scope > div:nth-child(4) > table > tbody > tr > td.task_body.col-md-8");
                if (task.Equals(true))
                {
                    Console.WriteLine("A subtask foi gerada");
                    throw new Exception("A subtask não deveria ser gerada pois os campos obrigatorios não foram preenchidos"); ;
                }
                else
                {
                    Console.WriteLine("A subtask não foi gerada");
                    Assert.IsFalse(false);

                }

            }

            catch (Exception e)
            {
                _common.LogFailedTest(e.ToString());
            }
        }

        [Test]
        public void Tc005CriarSubtaskPreenchendoCamposDescricaoComMaisDe250Caracteres()
        //Esse teste verifica se uma subbtask é criada mesmo inserindo mais de 250 caracteres
        //no campos de descrição
        {
            try
            {
                _ac.InserirEmail("rzdasil@gmail.com");
                _ac.InserirSenha("12345678");
                _ac.EntrarNoSistema();
                _ac.InserirTaskEnter("Teste2");
                _ac.ClicarNoManageSubtaskButton();
                _ac.InserirDescricaoSubtask("123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123412345dsdas");
                _ac.InserirDataVencimento("");
                _ac.ClicarEmAddSubtask();
                bool task = _element.IsElementPresentByCss("body > div.modal.fade.ng-isolate-scope.in > div > div > div.modal-body.ng-scope > div:nth-child(4) > table > tbody > tr > td.task_body.col-md-8");
                if (task.Equals(true))
                {
                    Console.WriteLine("A subtask foi gerada");
                    throw new Exception("A subtask não deveria ser gerada pois a descrição contém mais de 250 caracteres"); ;
                }
                else
                {
                    Console.WriteLine("A subtask não foi gerada");
                    Assert.IsFalse(false);

                }
            }

            catch (Exception e)
            {
                _common.LogFailedTest(e.ToString());
            }
        }
        [Test]
        public void Tc006CriarSubtaskPreenchendoFormatoDeDataInvalido()
        //Esse teste verifica se uma subtask é criada mesmo se inserismos no campo data um
        //formato de data errado.

        {
            try
            {
                _ac.InserirEmail("rzdasil@gmail.com");
                _ac.InserirSenha("12345678");
                _ac.EntrarNoSistema();
                _ac.InserirTaskEnter("Teste3");
                _ac.ClicarNoManageSubtaskButton();
                _ac.InserirDescricaoSubtask("teste");
                _ac.InserirDataVencimento("16/22/2017");
                _ac.ClicarEmAddSubtask();
                bool task = _element.IsElementPresentByCss("body > div.modal.fade.ng-isolate-scope.in > div > div > div.modal-body.ng-scope > div:nth-child(4) > table > tbody > tr > td.task_body.col-md-8");
                if (task.Equals(true))
                {
                    Console.WriteLine("A subtask foi gerada");
                    throw new Exception("A subtask não deveria ser gerada pois o formato de data"
                   + "inserida está errada");
                }
                else
                {
                    Console.WriteLine("A subtask não foi gerada");
                    Assert.IsFalse(false);

                }
            }

            catch (Exception e)
            {
                _common.LogFailedTest(e.ToString());
            }
        }
        [Test]
        public void Tc007TentarCriarSubtaskPressionandoEnterNoCampoDescricao()
            //Esse teste verifica se uma subtask é criada se acionarmos
            //a tecla enter do teclado (cursor no campo descrição) ao invés de clicarmos no botão "Add"

        {
            try
            {
                _ac.InserirEmail("rzdasil@gmail.com");
                _ac.InserirSenha("12345678");
                _ac.EntrarNoSistema();
                _ac.InserirTaskEnter("Teste4");
                _ac.ClicarNoManageSubtaskButton();
                _ac.InserirDescricaoSubtaskEnter("teste");       
                bool task = _element.IsElementPresentByCss("body > div.modal.fade.ng-isolate-scope.in > div > div > div.modal-body.ng-scope > div:nth-child(4) > table > tbody > tr > td.task_body.col-md-8");
                if (task.Equals(true))
                {
                    Console.WriteLine("A subtask foi gerada");
                    throw new Exception("A subtask não deveria ser gerada pois a unica forma de cria-la é acionando o botao ADD");
                }
                else
                {
                    Console.WriteLine("A subtask não foi gerada");
                    Assert.IsFalse(false);

                }
            }

            catch (Exception e)
            {
                _common.LogFailedTest(e.ToString());
            }
        }
        [Test]
        public void Tc008TentarCriarSubtaskPressionandoEnterNoCampoData()
        //Esse teste verifica se uma subtask é criada se acionarmos
        //a tecla enter do teclado (cursos no campo data) ao invés de clicarmos no botão "Add"
        {
            try
            {
                _ac.InserirEmail("rzdasil@gmail.com");
                _ac.InserirSenha("12345678");
                _ac.EntrarNoSistema();
                _ac.InserirTaskEnter("Teste5");
                _ac.ClicarNoManageSubtaskButton();
                _ac.InserirDataVencimentoEnter("26/01/2018");
                bool task = _element.IsElementPresentByCss("body > div.modal.fade.ng-isolate-scope.in > div > div > div.modal-body.ng-scope > div:nth-child(4) > table > tbody > tr > td.task_body.col-md-8");
                if (task.Equals(true))
                {
                    Console.WriteLine("A subtask foi gerada");
                    throw new Exception("A subtask não deveria ser gerada pois a unica forma de cria-la é acionando o botao ADD");
                }
                else
                {
                    Console.WriteLine("A subtask não foi gerada");
                    Assert.IsFalse(false);

                }
            }

            catch (Exception e)
            {
                _common.LogFailedTest(e.ToString());
            }
        }

        [Test]
        public void Tc009TentarAlterarTaskAPartirdoModalSubtask()
            //Esse teste verifica se é possível alterar uma task
            //a partir do modal diologue manage subtask

        {
            try
            {
                
                _ac.InserirEmail("rzdasil@gmail.com");
                _ac.InserirSenha("12345678");
                _ac.EntrarNoSistema();
                _ac.InserirTaskEnter("Teste5");
                _ac.ClicarNoManageSubtaskButton();
                var edittask = _element.FindById("edit_task", "campo editavel");
                if (edittask.Enabled)
                {
                    Console.WriteLine("A task foi alterada");
                    throw new Exception("O campo deveria estar bloqueado para edição");
                }
                else
                {
                    Console.WriteLine("O campo está bloqueado");
                }
            }

            catch (Exception e)
            {
                _common.LogFailedTest(e.ToString());
            }
        }

    }
}
