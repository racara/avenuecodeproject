﻿using System;
using System.IO;

namespace ProjetoAvenueCodeV2.Util
{
    class Files : ACBase
    {
        private StreamReader sr;

        public static void CreateNewFolder(String path)
        {
            System.IO.Directory.CreateDirectory(path);
        }

        public static void CreateResultFolder(String path)
        {
            CreateNewFolder(path);
            CreateNewFolder(path + @"\Passed");
            CreateNewFolder(path + @"\Failed");
        }

        public static void MoveFiles(String source, String target)
        {
            System.IO.File.Move(source, target);
        }

        public static void CopyFiles(String source, String target)
        {
            System.IO.File.Copy(source, target);
        }

        public String GetNumberOfLinesInFile(String file)
        {
            sr = new StreamReader(file, true);
            String line;
            int count = 0;
            while (!sr.EndOfStream)
            {
                line = sr.ReadLine();
                Console.WriteLine("Linha " + count + " = " + line);
                if (!line.Equals(";") && !line.Contains("-------------------"))
                {
                    count++;
                }
            }
            if (count != 0)
            {
                count--;
            }
            Console.WriteLine("Número de linhas no arquivo " + file + " é igual a " + count);
            return count.ToString();
        }

        public String GetLoteFolderInHistorico(String path, String lote)
        {
            try
            {
                String[] paths = System.IO.Directory.GetDirectories(path + @"Historico");
                for (int i = 0; i < paths.Length; i++)
                {
                    if (paths[i].Contains(lote))
                    {
                        Console.WriteLine("Nome da pasta encontrada é " + paths[i]);
                        return paths[i];
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exceção " + e);
            }
            return null;
        }
    }
}
