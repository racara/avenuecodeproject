﻿using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;

namespace ProjetoAvenueCodeV2.Util
{
    class Logger : ACBase
    {
        private static StreamReader _sr;
        private StreamWriter _sw;
        private readonly String _path;

        public Logger(String path)
        {
            _path = path;
        }

        public void WriteLogLine(String message, String details, Boolean pass)
        {
            string result = pass ? "<font color=\"#FFF\"><td bgcolor='#98FB98'>PASS</td></font>" : "<td bgcolor='#FF6347' align='center'><font color=\"#FFF\">FAIL</font></td>";

            List<String> list = GetLogLines(_path);
            _sw = new StreamWriter(_path, false, Encoding.UTF8);
            for (int i = 0; i < list.Count; i++)
            {
                _sw.WriteLine(list[i]);
            }
            _sw.WriteLine("<tr>");
            _sw.WriteLine("<td>" + message + "</td><td>" + details + "</td>" + result);
            _sw.WriteLine("</tr>");
            _sw.Close();
        }

        public void CreateIndividualTestLog(String path)
        {
            _sw = new StreamWriter(path, false, Encoding.UTF8);
            _sw.WriteLine("<html>");
            _sw.WriteLine("<head>");

            _sw.WriteLine("<style type=\"text/css\">");
            _sw.WriteLine("</style>");
            _sw.WriteLine("</head>");
            _sw.WriteLine("<body>");
            _sw.WriteLine("<table align=\"left\" border=\"0\" cellspacing=\"1\">");

            _sw.WriteLine("<tr> <td colspan=\"3\"> <h2>Log do teste: " + TestContext.CurrentContext.Test.Name + "</h2> </td></tr>");
            _sw.WriteLine("<thead>");
            _sw.WriteLine("<td colspan=\"3\"> Caminho do Arquivo: " + path + "</td><td></td><td></td>");
            _sw.WriteLine("</thead>");
            _sw.Close();
        }

        public void CloseIndividualTestLog(String path)
        {
            try
            {
                List<String> list = GetLogLines(path);
                _sw = new StreamWriter(path, false, Encoding.UTF8);
                for (int i = 0; i < list.Count; i++)
                {
                    _sw.WriteLine(list[i]);
                }
                String testName = TestContext.CurrentContext.Test.Name;
                String result = TestContext.CurrentContext.Result.ToString();
                _sw.WriteLine("<tr>");
                if (result.Equals("Passed"))
                {
                    _sw.WriteLine("<td><b>" + testName + "</b></td>" + "<td></td><td bgcolor='#98FB98'><b>" + result + "</b></td>");
                }
                else
                {
                    _sw.WriteLine("<td><b>" + testName + "</b></td>" + "<td></td><td bgcolor='#FA8072'><b>" + result + "</b></td>");
                }


                _sw.WriteLine("</tr>");
                _sw.WriteLine("<tr>");
                _sw.WriteLine("<td BGCOLOR=\"#528B8B\" colspan=\"3\"><font color=\"#FFF\"> © 2014 Crivo Workflow </font></td>");
                _sw.WriteLine("</tr>");

                _sw.WriteLine("</table>");
                _sw.WriteLine("</body>");
                _sw.WriteLine("</html>");
                _sw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Não foi possível concluir o arquivo de Log. 'CloseIndividualTestLog' Erro:" + e);
            }
        }

        public List<String> GetLogLines(String path)
        {
            var list = new List<String>();
            _sr = new StreamReader(path);
            while (!_sr.EndOfStream)
            {
                list.Add(_sr.ReadLine());
            }
            _sr.Close();
            return list;
        }

        public void MoveLogFiles(String logFolder)
        {
            try
            {
                String result = TestContext.CurrentContext.Result.ToString();

                Files.MoveFiles(_path,
                                result.Equals("Passed")
                                    ? _path.Replace(logFolder, logFolder + @"\Passed\")
                                    : _path.Replace(logFolder, logFolder + @"\Failed\"));
            }
            catch (Exception e)
            {
                Console.WriteLine("Não foi possível mover o arquivo de Log. Erro:" + e);
            }
        }

        public void CreateExecutionResult(String logFolder)
        {
            try
            {
                String[] logsPassed = Directory.GetFiles(logFolder + @"\Passed");
                String[] logsFailed = Directory.GetFiles(logFolder + @"\Failed");

                _sw = new StreamWriter(logFolder + "\\" + TestContext.CurrentContext.Test.Name

                                       + ".html", false, Encoding.UTF8);
                _sw.WriteLine("<html>");
                _sw.WriteLine("<head>");

                _sw.WriteLine("<style type=\"text/css\">");
                _sw.WriteLine("body {");
                _sw.WriteLine("color: #65797D;");
                _sw.WriteLine("font-family: Arial,Helvetica,sans-serif;");
                _sw.WriteLine("}");
                _sw.WriteLine("table{");
                _sw.WriteLine("position:relative;");
                _sw.WriteLine("}");
                _sw.WriteLine("tr{");
                _sw.WriteLine("background-color:EDF0D1");
                _sw.WriteLine("color:white;");
                _sw.WriteLine("}");
                _sw.WriteLine("</style>");
                _sw.WriteLine("</head>");
                _sw.WriteLine("<body>");
                _sw.WriteLine("<tr><td colspan=\"3\">  </td>");

                _sw.WriteLine("<div id=\"div2\">");
                _sw.WriteLine("<h1>Execution log</h1>");
                _sw.WriteLine("<br>");
                _sw.WriteLine("<h2>Total of Tests Executed: " + (logsPassed.Length + logsFailed.Length) + "</h2>");
                _sw.WriteLine("</div>");
                _sw.WriteLine("<div height=\"50px\">");
                _sw.WriteLine("<table>");
                _sw.WriteLine("<tr>");
                _sw.WriteLine("<td><b>Succeded - " + logsPassed.Length + " test(s)</b></td><td></td><td></td>");
                _sw.WriteLine("</tr>");

                foreach (string logsP in logsPassed)
                {
                    _sw.WriteLine("<tr>");
                    _sw.WriteLine("<td><a href=\"" + logsP + "\" target=\"_blank\">" + logsP + "</a></td><td></td><td></td>");
                    _sw.WriteLine("</tr>");
                }

                _sw.WriteLine("<tr bgcolor='#F5F5F5'><td></td></tr>");
                _sw.WriteLine("<tr bgcolor='#F5F5F5'><td></td></tr>");
                _sw.WriteLine("<tr>");
                _sw.WriteLine("<td><b>Failed - " + logsFailed.Length + " test(s)</b></td><td></td><td></td>");
                _sw.WriteLine("</tr>");

                foreach (string logsF in logsFailed)
                {
                    _sw.WriteLine("<tr>");
                    _sw.WriteLine("<td><a href=\"" + logsF + "\" target=\"_blank\">" + logsF + "</a></td><td></td><td></td>");
                    _sw.WriteLine("</tr>");
                }

                _sw.WriteLine("</table>");
                _sw.WriteLine("</body>");
                _sw.WriteLine("</html>");
                _sw.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine("Não foi possível escrever no arquivo de Log. 'CreateExecutionResult' Erro:" + e);
            }
        }
    }
}
