﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;


namespace ProjetoAvenueCodeV2.Util
{
    class Element : ACBase
    {
        protected static Logger Log;
        public IWebElement Find(By by, String labelElementLog)
        {
            for (int i = 0; i <= 10; i++)
            {
                try
                {
                    return Selenium.FindElement(by);
                }
                catch (NoSuchElementException e)
                {
                    if (i == 10)
                    {
                        Log.WriteLogLine("Element <b>'" + labelElementLog + "'</b> not found", e.Message, false);
                    }
                }
                Thread.Sleep(1200);
            }
            return null;
        }

        public IWebElement Find(IWebElement parent, By by, String labelElementLog)
        {
            for (int i = 0; i <= 10; i++)
            {
                try
                {
                    return parent.FindElement(by);
                }
                catch (NoSuchElementException e)
                {
                    if (i == 10)
                    {
                        Log.WriteLogLine("Element <b>'" + labelElementLog + "'</b> not found", e.Message, false);
                    }
                }
                Thread.Sleep(500);
            }
            return null;
        }

        public IWebElement FindById(string id, String labelElementLog)
        {
            return Find(By.Id(id), labelElementLog);
        }

        public IWebElement FindByName(String name, String labelElementLog)
        {
            return Find(By.Name(name), labelElementLog);
        }

        public IWebElement FindByName(IWebElement parent, String name, String labelElementLog)
        {
            return Find(parent, By.Name(name), labelElementLog);
        }

        public IWebElement FindByText(String text)
        {
            return Find(By.LinkText(text), text);
        }

        public IWebElement FindByClassName(String className, String labelElementLog)
        {
            return Find(By.ClassName(className), labelElementLog);
        }

        public IWebElement FindByClassName(IWebElement parent, String className, String labelElementLog)
        {
            return Find(parent, By.ClassName(className), labelElementLog);
        }

        public IList<IWebElement> FindElementsByClassName(String className)
        {
            return Selenium.FindElements(By.ClassName(className));
        }

        public IWebElement FindByCss(String cssName, String labelElementLog)
        {
            return Find(By.CssSelector(cssName), labelElementLog);
        }

        public IWebElement FindByXPath(String xPath, String labelElementLog)
        {
            return Find(By.XPath(xPath), labelElementLog);
        }

        public ReadOnlyCollection<IWebElement> FindByXPath(String xPath)
        {
            for (int i = 0; i <= 10; i++)
            {
                try
                {
                    return Selenium.FindElements(By.XPath(xPath));
                }
                catch (NoSuchElementException e)
                {
                    if (i == 10)
                    {
                        Log.WriteLogLine("Element <b>'" + xPath + "'</b> not found", e.Message, false);
                    }
                }
                Thread.Sleep(500);
            }
            return null;
        }

        public ReadOnlyCollection<IWebElement> FindByXPath(IWebElement parent, String xPath)
        {
            for (int i = 0; i <= 10; i++)
            {
                try
                {
                    return parent.FindElements(By.XPath(xPath));
                }
                catch (NoSuchElementException e)
                {
                    if (i == 10)
                    {
                        Log.WriteLogLine("Element <b>'" + xPath + "'</b> not found", e.Message, false);
                    }
                }
                Thread.Sleep(500);
            }
            return null;
        }

        public void SetText(IWebElement element, String text, String labelElementLog)
        {
            Thread.Sleep(150);
            element.Clear();
            element.SendKeys(text);
            Log.WriteLogLine("Set text <b>'" + text + "'</b> to the field <b>'" + labelElementLog + "'</b>",
                             "Text <b>'" + text + "'</b> was set to the element: <b>'" + labelElementLog + "'</b>", true);
        }

        public void SetTextAndEnter(IWebElement element, String textField, String labelElementLog)
        {
            element.Clear();
            element.SendKeys(textField);
            Log.WriteLogLine("Set text <b>'" + textField + "'</b> to the field <b>'" + labelElementLog + "'</b>",
                            "Text <b>'" + textField + "'</b> was set to the element: <b>'" + labelElementLog + "'</b>", true);
            element.SendKeys(Keys.Enter);
        }

        public void SetTextAndTab(IWebElement element, String textField, String labelElementLog)
        {
            element.Clear();
            element.SendKeys(textField);
            Log.WriteLogLine("Set text <b>'" + textField + "'</b> to the field <b>'" + labelElementLog + "'</b>",
                            "Text <b>'" + textField + "'</b> was set to the element: <b>'" + labelElementLog + "'</b>", true);
            element.SendKeys(Keys.Tab);
        }

        public void SetTextsAndEnter(IWebElement element, String textField, String labelElementLog)
        {

            element.SendKeys(textField);
            Log.WriteLogLine("Set text <b>'" + textField + "'</b> to the field <b>'" + labelElementLog + "'</b>",
                             "Text <b>'" + textField + "'</b> was set to the element: <b>'" + labelElementLog + "'</b>", true);
            element.SendKeys(Keys.Enter);


        }

        public void Click(IWebElement element, string labelElementLog)
        {
            Thread.Sleep(2000);
            element.Click();
            Thread.Sleep(2000);
            //   Log.WriteLogLine("Click <b>'" + labelElementLog + "'</b>", "The element <b>'" + labelElementLog + "'</b> was clicked", true);
        }

        public void SelectByValue(IWebElement element, String value, String labelElementLog)
        {
            try
            {
                var selectElement = new SelectElement(element);

                selectElement.SelectByValue(value);
                Log.WriteLogLine("Select Value <b>'" + value + "'</b> for field <b>'" + labelElementLog + "'</b>",
                                 "The value <b>'" + value + "'</b> was selected", true);
            }
            catch (NoSuchElementException e)
            {
                Log.WriteLogLine("Select Value <b>'" + value + "'</b> for field <b>'" + labelElementLog + "'</b>", e.Message, false);
            }
        }

        public void SelectByText(IWebElement element, String value, String labelElementLog)
        {
            try
            {
                var selectElement = new SelectElement(element);

                selectElement.SelectByText(value);
                Log.WriteLogLine("Select Value <b>'" + value + "'</b> for field <b>'" + labelElementLog + "'</b>",
                                 "The value <b>'" + value + "'</b> was selected", true);
            }
            catch (NoSuchElementException e)
            {
                Log.WriteLogLine("Select Value <b>'" + value + "'</b> for field <b>'" + labelElementLog + "'</b>", e.Message, false);
            }
        }

        public void SelectByIndex(IWebElement element, int value, String labelElementLog)
        {
            try
            {
                var selectElement = new SelectElement(element);

                selectElement.SelectByIndex(value);
                Log.WriteLogLine("Select Value <b>'" + value + "'</b> for field <b>'" + labelElementLog + "'</b>",
                                 "The value <b>'" + value + "'</b> was selected", true);
            }
            catch (NoSuchElementException e)
            {
                Log.WriteLogLine("Select Value <b>'" + value + "'</b> for field <b>'" + labelElementLog + "'</b>", e.Message, false);
            }
        }

        public Boolean IsElementPresentById(string idElement)
        {
            try
            {
                Selenium.FindElement(By.Id(idElement));
                return true;
            }
            catch (NoSuchElementException e)
            {
                return false;
            }
        }

        public Boolean IsElementPresentByText(string text)
        {
            try
            {
                Selenium.FindElement(By.LinkText(text));
                return true;
            }
            catch (NoSuchElementException e)
            {
                return false;
            }
        }

        public Boolean IsElementPresentByClass(string classElement)
        {
            try
            {
                Selenium.FindElement(By.ClassName(classElement));
                return true;
            }
            catch (NoSuchElementException e)
            {
                return false;
            }
        }

        public Boolean IsElementPresentByCss(string cssElement)
        {
            try
            {
                Selenium.FindElement(By.CssSelector(cssElement));
                return true;
            }
            catch (NoSuchElementException e)
            {
                return false;
            }

        }

        public Boolean IsElementPresentByXpath(string xpathElement)
        {
            try
            {
                Selenium.FindElements(By.XPath(xpathElement));
                return true;
            }
            catch (NoSuchElementException e)
            {
                return false;
            }

        }

        public IWebElement FindElementByLinkText(String text, String labelElementLog)
        {
            for (int i = 0; i <= 10; i++)
            {
                try
                {
                    return Selenium.FindElement(By.LinkText(text));
                }
                catch (NoSuchElementException e)
                {
                    if (i == 10)
                    {
                        Log.WriteLogLine("Link <b>'" + labelElementLog + "'</b> (Text=" + text + ") not found", e.Message, false);

                    }
                }
                Thread.Sleep(500);
            }
            return null;
        }

        public String ReturnCurrentUrl()
        {
            var url = Selenium.Url;
            return url;
        }

        public Boolean VerifyCodePageContains(string msg)
        {
            var elementFound = Selenium.PageSource.Contains(msg);
            return elementFound;
        }

        public String GetCurrentWindowHandle()
        {
            return Selenium.CurrentWindowHandle;
        }
    }
}
