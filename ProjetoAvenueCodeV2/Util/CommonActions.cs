﻿using System;
using System.Drawing;
using System.Threading;
using System.IO;
using NUnit.Framework;
using OpenQA.Selenium;

namespace ProjetoAvenueCodeV2.Util
{
    class CommonActions : ACBase
    {
        private Element _element;
        private Logger Log;

        public CommonActions()
        {
            Log = new Logger("");
        }


        public void HighightElement(IWebElement element)
        {
            for (int i = 0; i < 2; i++)
            {
                var js = (IJavaScriptExecutor)Selenium;
                js.ExecuteScript("arguments[0].setAttribute('style', arguments[1]);", element,
                    "color: yellow; border: 2px solid yellow;");
                js.ExecuteScript("arguments[0].setAttribute('style', arguments[1]);", element, "");
            }
        }

        public void SwitchCurrentWindow()
        {
            //--- Antes de alterar o foco para um pop up o handle anterior é guardado na variável que está na Base CurrentWindow
            Selenium.SwitchTo().Window(CurrentWindow);
        }

        public static void GoToUrl(string url)
        {
            Selenium.Navigate().GoToUrl(url);
            Console.WriteLine("Acessando url.." + url);
        }

        public void CloseWindow()
        {
            Selenium.Close();
        }

        public string GeraString(int min, int max)
        {
            string guid = Guid.NewGuid().ToString().Replace("-", "");

            var clsRan = new Random();
            Int32 tamanhoSenha = clsRan.Next(min, max);

            string senha = "";
            for (Int32 i = 0; i <= tamanhoSenha; i++)
            {
                senha += guid.Substring(clsRan.Next(1, guid.Length), 1);
            }

            return senha;
        }

        public Boolean VerifyElementContaisClass(string elementId, string classExpected)
        {
            IWebElement elemento = _element.FindById(elementId, "procurando elemento...");
            var classField = elemento.GetAttribute("class");

            if (classField.Contains(classExpected))
            {
                return true;
            }
            return false;
        }

        public void SaveScreenshotWithHighlightedElement(Screenshot screenshot, IWebElement element, string filename)
        {
            using (var image = Bitmap.FromStream(new MemoryStream(screenshot.AsByteArray)))
            {
                using (var graphics = Graphics.FromImage(image))
                {
                    graphics.DrawRectangle(new Pen(Color.Red, 10), new Rectangle(element.Location, element.Size));
                }

                image.Save(filename);
            }
        }

        public void WaitLoadingFilters()
        {
            Boolean msg = true;
            int Timeout = 6;
            int count = 0;
            while (msg)
            {
                var elemento = _element.FindByCss("span.ng-hide", "Loading Filter");

                if (elemento.Displayed && count < Timeout)
                {
                    Thread.Sleep(1000);
                    count += 1;
                }
                else
                {
                    msg = false;
                }
            }
            Thread.Sleep(1500);

        }

        public void WaitProgressMessage()
        {
            Boolean progress = true;
            int Timeout = 120000;
            int count = 0;
            while (progress)
            {
                if (_element.FindById("ajaxProgressMessage", "Progress Dialog").Displayed && count < Timeout)
                {
                    Thread.Sleep(3000);
                    count += 2000;
                }
                else
                {
                    progress = false;
                }
            }
            Thread.Sleep(3000);
        }

        public void WaitElementAppearWithTimeout(string elementId, int timeout)
        {
            int count = 0;
            do
            {
                Thread.Sleep(1500);
                count++;
            } while (_element.IsElementPresentById(elementId) == false && count < timeout);
        }

        public void LogStartTest(string descriptionTest)
        {
            Log.WriteLogLine("</td><td></td><td></td></tr><tr BGCOLOR=\"#B4CDCD\"><td>" +
                             "<h3>TESTE INICIADO </h3><br/></td>" +
                             "<td><h3>Objetivo do Teste: " + descriptionTest + " </h3>" +
                             "<br/><b>" +
                             "</b></td><td></td></tr>" +
                             "<tr><td>", " ", true);
        }

        public void LogEndTest(bool haveAfterTest)
        {
            Log.WriteLogLine("</td><td></td><td></td></tr><tr BGCOLOR=\"#B4CDCD\"><td>" +
                             "<h3>TESTE FINALIZADO</h3></td>" +
                             "<td><b>Resultado esperado obtido com sucesso.</b></td><td></td></tr>" +
                             "<tr><td>", " ", true);

            if (haveAfterTest)
            {
                Log.WriteLogLine("INICIANDO pós teste: ", " Excluindo configuração criadas ou excluindo BP... ", true);
            }
        }

        public void LogFailedTest(string exeption)
        {
            Log.WriteLogLine("</td><td></td><td></td></tr><tr BGCOLOR=\"#FA8072\"><td>" +
                             "<h3>TESTE FALHOU. </h3></td>" +
                             "<td></td><td></td></tr><tr><td>", " ", false);


            Log.WriteLogLine("Detalhes do Erro:", exeption, false);
            Assert.Fail();
        }

        public string Alert(bool acceptNextAlert)
        {
            IAlert alert = Selenium.SwitchTo().Alert();
            string alertText = alert.Text;
            if (acceptNextAlert)
            {
                alert.Accept();
            }
            else
            {
                alert.Dismiss();
            }

            return alertText;
        }

        public void PreencherDatePickerInicial(String dataInicial)
        {
            String dia, mes, mesString, ano, hora, minuto, segundo;

            //variaveis com os dados necessarios a partir do digitado pelo usuario
            dia = dataInicial.Substring(0, 2);
            mes = dataInicial.Substring(3, 2);
            ano = dataInicial.Substring(6, 4);
            hora = dataInicial.Substring(11, 2);
            minuto = dataInicial.Substring(14, 2);
            segundo = dataInicial.Substring(17, 2);

            mesString = RetornaMesdt(mes);
            _element.Click(_element.FindByXPath("//input[@id='txtIntervalStart']", "Campo Data"), "data");
            Thread.Sleep(3000);
            SelecionarAno(ano);
            SelecionarMes(mesString);
            SelecionarDia(dia);

            PreencherHoraInicial(hora);
            PreencherMinutoInicial(minuto);
            PreencherSegundoInicial(segundo);
            _element.Click(_element.FindByXPath("//*[@id='ui-datepicker-div']/div[3]/button[2]", "fechar"), "fechar");
        }
        public String RetornaMesdt(String mes)
        {
            String mesTexto;
            switch (mes)
            {
                case "01": mesTexto = "Jan"; break;
                case "02": mesTexto = "Fev"; break;
                case "03": mesTexto = "Mar"; break;
                case "04": mesTexto = "Abr"; break;
                case "05": mesTexto = "Mai"; break;
                case "06": mesTexto = "Jun"; break;
                case "07": mesTexto = "Jul"; break;
                case "08": mesTexto = "Ago"; break;
                case "09": mesTexto = "Set"; break;
                case "10": mesTexto = "Out"; break;
                case "11": mesTexto = "Nov"; break;
                default: mesTexto = "Dez"; break;
            }
            return mesTexto;
        }

        public void PreencherDatePickerFinal(String dataInicial)
        {
            String dia, mes, mesString, ano, hora, minuto, segundo;

            //variaveis com os dados necessarios a partir do digitado pelo usuario
            dia = dataInicial.Substring(0, 2);
            mes = dataInicial.Substring(3, 2);
            ano = dataInicial.Substring(6, 4);
            hora = dataInicial.Substring(11, 2);
            minuto = dataInicial.Substring(14, 2);
            segundo = dataInicial.Substring(17, 2);

            mesString = RetornaMesdt(mes);
            _element.Click(_element.FindByXPath("//*[@id='txtIntervalFinal']", "Campo Data"), "data");
            Thread.Sleep(3000);
            SelecionarAno(ano);
            SelecionarMes(mesString);
            SelecionarDia(dia);



            PreencherHoraFinal(hora);
            PreencherMinutoFinal(minuto);
            PreencherSegundoFinal(segundo);
        }
        public String RetornaMes(String mes)
        {
            String mesTexto;
            switch (mes)
            {
                case "01": mesTexto = "Jan"; break;
                case "02": mesTexto = "Fev"; break;
                case "03": mesTexto = "Mar"; break;
                case "04": mesTexto = "Abr"; break;
                case "05": mesTexto = "Mai"; break;
                case "06": mesTexto = "Jun"; break;
                case "07": mesTexto = "Jul"; break;
                case "08": mesTexto = "Ago"; break;
                case "09": mesTexto = "Set"; break;
                case "10": mesTexto = "Out"; break;
                case "11": mesTexto = "Nov"; break;
                default: mesTexto = "Dez"; break;
            }
            return mesTexto;
        }

        public void SelecionarAno(String ano)
        {
            _element.SelectByText(_element.FindByCss("select.ui-datepicker-year", "Ano"), ano, "ano");
        }

        public void SelecionarMes(String mes)
        {
            _element.SelectByText(_element.FindByCss("select.ui-datepicker-month", "mes"), mes, "mes");
        }
        public void SelecionarDia(String dia)
        {
            _element.Click(_element.FindByXPath("//a[contains(text(),'" + dia + "')]", "dia"), "dia");
        }
        public void PreencherHoraInicial(String hora)
        {
            //selenium.Type("ui_tpicker_hour_textBoxInitialDate", hora);
            _element.SetText(_element.FindByXPath("//input[@id='ui_tpicker_hour_txtIntervalStart']", "hora"), hora, "hora");
        }

        public void PreencherMinutoInicial(String minuto)
        {
            int minutos = Convert.ToInt32(minuto) + 3;
            string sminuto = Convert.ToString(minutos);
            _element.SetText(_element.FindByXPath("//input[@id='ui_tpicker_minute_txtIntervalStart']", "minutos"), sminuto, "minutoa");
        }

        public void PreencherSegundoInicial(String segundo)
        {
            _element.SetText(_element.FindByXPath("//input[@id='ui_tpicker_second_txtIntervalStart']", "SEGUNDOS"), segundo, "segundos");
        }

        public void PreencherHoraFinal(String hora)
        {
            //selenium.Type("ui_tpicker_hour_textBoxInitialDate", hora);
            _element.SetText(_element.FindByXPath("//*[@id='ui_tpicker_hour_txtIntervalFinal']", "hora"), hora, "hora");
        }

        public void PreencherMinutoFinal(String minuto)
        {
            int minutos = Convert.ToInt32(minuto) + 3;
            string sminuto = Convert.ToString(minutos);
            _element.SetText(_element.FindByXPath("//*[@id='ui_tpicker_minute_txtIntervalFinal']", "minutos"), sminuto, "minutoa");
        }

        public void PreencherSegundoFinal(String segundo)
        {
            _element.SetText(_element.FindByXPath("//*[@id='ui_tpicker_second_txtIntervalFinal']", "SEGUNDOS"), segundo, "segundos");
        }

        public void PreencherDatePickerInicioAgendamento(String dataInicial)
        {
            String dia, mes, mesString, ano, hora, minuto, segundo;

            //variaveis com os dados necessarios a partir do digitado pelo usuario
            dia = dataInicial.Substring(0, 2);
            mes = dataInicial.Substring(3, 2);
            ano = dataInicial.Substring(6, 4);
            hora = dataInicial.Substring(11, 2);
            minuto = dataInicial.Substring(14, 2);
            segundo = dataInicial.Substring(17, 2);

            mesString = RetornaMesdt(mes);
            _element.Click(_element.FindByXPath("//*[@id='txtSchedulingStart']", "Campo Data"), "data");
            Thread.Sleep(3000);
            SelecionarAno(ano);
            SelecionarMes(mesString);
            SelecionarDia(dia);

            PreencherHoraInicialAgendamento(hora);
            PreencherMinutoInicialAgendamento(minuto);
            PreencherSegundoInicialAgendamento(segundo);
            _element.Click(_element.FindByXPath("//*[@id='ui-datepicker-div']/div[3]/button[2]", "fechar"), "fechar");
        }


        public void PreencherHoraInicialAgendamento(String hora)
        {
            //selenium.Type("ui_tpicker_hour_textBoxInitialDate", hora);
            _element.SetText(_element.FindByXPath("//*[@id='ui_tpicker_hour_txtSchedulingStart']", "hora"), hora, "hora");
        }

        public void PreencherMinutoInicialAgendamento(String minuto)
        {
            _element.SetText(_element.FindByXPath("//*[@id='ui_tpicker_minute_txtSchedulingStart']", "minutos"), minuto, "minutoa");
        }

        public void PreencherSegundoInicialAgendamento(String segundo)
        {
            _element.SetText(_element.FindByXPath("//*[@id='ui_tpicker_second_txtSchedulingStart']", "SEGUNDOS"), segundo, "segundos");
        }


        //Lembrete
        public void PreencherHoraDoLembrete(String horalembrete)
        {
            String hora, minuto, segundo; ;

            //variaveis com os dados necessarios a partir do digitado pelo usuario
            hora = horalembrete.Substring(0, 2);
            minuto = horalembrete.Substring(3, 2);
            segundo = horalembrete.Substring(6, 2);



            PreencherHoraLembrete(hora);
            PreencherMinutoLembrete(minuto);
            PreencherSegundoLembrete(segundo);
            _element.Click(_element.FindByXPath("//*[@id='ui-datepicker-div']/div[3]/button[2]", "fechar"), "fechar");
        }

        public void PreencherHoraLembrete(String hora)
        {
            //selenium.Type("ui_tpicker_hour_textBoxInitialDate", hora);
            _element.SetText(_element.FindById("ui_tpicker_hour_textReminderExpireDate", "hora"), hora, "hora");
        }

        public void PreencherMinutoLembrete(String minuto)
        {
            int minutos = Convert.ToInt32(minuto) + 3;
            string sminuto = Convert.ToString(minutos);
            _element.SetText(_element.FindById("ui_tpicker_minute_textReminderExpireDate", "minutos"), sminuto, "minutoa");
        }

        public void PreencherSegundoLembrete(String segundo)
        {
            _element.SetText(_element.FindById("ui_tpicker_second_textReminderExpireDate", "SEGUNDOS"), segundo, "segundos");
        }
    }
}
